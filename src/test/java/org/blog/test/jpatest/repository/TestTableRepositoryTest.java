package org.blog.test.jpatest.repository;

import org.blog.test.jpatest.entity.TestTable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TestTableRepositoryTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private TestTableRepository testRepository;

    @Test
    public void findByName() {
        //Given
        TestTable testTable = new TestTable("test");
        this.testEntityManager.persist(testTable);

        //When
        List<TestTable> result = testRepository.findByName("test");

        //Then
        assertThat(result.get(0), is(testTable));
    }
}
