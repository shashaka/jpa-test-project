package org.blog.test.jpatest.repository;

import org.blog.test.jpatest.entity.TestTable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TestTableRepository extends JpaRepository<TestTable, Long> {
    List<TestTable> findByName(String name);
}
